
export interface IGameSettings {
    shellsNumber: number;
    speed: number;
  }