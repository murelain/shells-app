import { useCallback, useEffect, useState } from "react";
import "./App.scss";
import GameSettings from "./components/GameSettings/GameSettings";
import Playground from "./components/Playgound/Playgound";
import { DEFAULT_NUMBER_OF_SHELLS, DEFAULT_SPEED } from "./constants";
import { IGameSettings } from "./interfaces";

function App() {
  const [gameSettings, setGameSettings] = useState<IGameSettings>({
    shellsNumber: DEFAULT_NUMBER_OF_SHELLS,
    speed: DEFAULT_SPEED,
  });
  const [shells, setShells] = useState<number | undefined>();

  const [shuffleState, setShuffleState] = useState(false);
  const [ballPosition, setBallPosition] = useState(-1);
  const [gameOver, setGameOver] = useState(true);

  const handleStart = useCallback(() => {
    setGameOver(true);
  }, []);

  const handleSettingsChange = (settings: IGameSettings) => {
    setGameSettings(settings);
  };

  const handleOnClick = (value: boolean) => {
    setGameOver(true);
    alert(value ? "You found the ball!" : "Oops! not here");
  };

  useEffect(() => {
    setGameOver(true);
    setShuffleState(false);
  }, [shells]);

  useEffect(() => {
    const newBallPosition = Math.floor(
      Math.round(Math.random() * (gameSettings.shellsNumber - 1))
    );
    setBallPosition(newBallPosition);
    setShells(gameSettings.shellsNumber);
  }, [gameSettings.shellsNumber]);

  const handleShuffle = () => {
    setShuffleState(true);
    setGameOver(false);
    setTimeout(() => {
      setShuffleState(false);
    }, 10);
  };

  return (
    <div className="App">
      <header className="App-header">
        <GameSettings
          onSettingsChange={(settings: IGameSettings) =>
            handleSettingsChange(settings)
          }
        />
        <div className='control-buttons'>
          <button onClick={handleStart}>Reset</button>
          <button onClick={handleShuffle}>Shuffle</button>
        </div>
        {shells && (
          <Playground
            gameOver={gameOver}
            ballPosition={ballPosition}
            shuffleState={shuffleState}
            onShellClick={handleOnClick}
            settings={gameSettings}
          />
        )}
        <div></div>
      </header>
    </div>
  );
}

export default App;
