import React, { FC, useEffect, useState } from "react";
import { SHELL_WIDTH } from "../../constants";
import { IGameSettings } from "../../interfaces";
import Shell from "../Shell/Shell";

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const Playground: FC<{
  settings: IGameSettings;
  ballPosition: number;
  shuffleState: boolean;
  gameOver: boolean;
  onShellClick: (ballFound: boolean) => void;
}> = ({ settings, ballPosition, shuffleState, gameOver, onShellClick }) => {
  const [shells, setShells] = useState([]);
  const [refs, setRefs] = useState<
    { [key: number]: React.RefObject<HTMLElement> } | {}
  >({});

  useEffect(() => {
    const newShellsNumber = Array.from({ length: settings.shellsNumber });
    setShells(newShellsNumber);

    const shellRefs = newShellsNumber?.reduce((refs, value, i) => {
      refs[i] = React.createRef();
      return refs;
    }, {});

    setRefs(shellRefs);
  }, [settings]);

  useEffect(() => {
    if (shuffleState) {
      const newOrder = Object.keys(refs).sort(
        () => getRandomIntInclusive(-1, 1) - 0.3
      );

      newOrder.forEach((newOrderKey, i) => {
        const newOrderPosition = (+newOrderKey - i) * SHELL_WIDTH;
        const speedK = 1 / settings.speed;
        const randY = getRandomIntInclusive(-4, 4) * 10;
        const styleString = `transform: translate(${newOrderPosition}px, ${randY}px); transition: all ${speedK}s ease-in;`;
        refs[i].current.setAttribute("style", styleString);

        setTimeout(() => {
          const speedK = 0.5 * settings.speed;
          const styleString = `transform: translate(${newOrderPosition}px, 0); transition: all ${speedK}s ease-in;`;
          refs[i].current.setAttribute("style", styleString);
        }, (1 / settings.speed) * 1000);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shuffleState, refs]);

  return (
    <div className="playgound">
      {shells.map((_, i) => (
        <div className="shell-container" key={i} ref={refs[i]}>
          <Shell
            hasBall={ballPosition === i}
            gameOver={gameOver}
            onClick={onShellClick}
          />
        </div>
      ))}
    </div>
  );
};

export default Playground;
