import { FC, useEffect, useState } from "react";
import { DEFAULT_NUMBER_OF_SHELLS } from "../../constants";
import { IGameSettings } from "../../interfaces";

const GameSettings: FC<{
  onSettingsChange: (settings: IGameSettings) => void;
}> = ({ onSettingsChange }) => {
  const [shellsNumber, setShellsNumber] = useState<number>(DEFAULT_NUMBER_OF_SHELLS);
  const [speed, setSpeed] = useState(1);

  const handleNumberChange = (newValue: number) => {
    setShellsNumber(newValue);
  };

  const handleSpeedChange = (newValue: number) => {
    setSpeed(+newValue);
  };

  useEffect(() => {
    if (!shellsNumber) return;
    onSettingsChange({ shellsNumber, speed });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shellsNumber, speed]);

  return (
    <>
      <div className="input-group">
        <label>Number of Shells</label>
        <input
          type="number"
          placeholder="Number of shells"
          onChange={(newValue) => handleNumberChange(+newValue.target.value)}
          value={shellsNumber}
        />
      </div>
      <div className="input-group">
        <label>Speed</label>
        <input
          type="number"
          min="1"
          placeholder="Speed"
          onChange={(newValue) => handleSpeedChange(+newValue.target.value)}
          value={speed}
        />
      </div>
    </>
  );
};

export default GameSettings;
