import { FC } from "react";

const Shell: FC<{
  hasBall: boolean;
  gameOver: boolean;
  onClick: (ballFound: boolean) => void;
}> = ({ hasBall, gameOver, onClick }) => {
  const handleClick = () => {
    onClick(hasBall);
  };
  return (
    <div className={`shell ${hasBall && gameOver ? "opened" : ''}`} onClick={handleClick}>
      {hasBall && <div className={`ball ${!gameOver ? "hidden" : ''}`}></div>}
    </div>
  );
};

export default Shell;
